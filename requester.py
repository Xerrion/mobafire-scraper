import requests


class Requester:
    @staticmethod
    def get_guides(champion="", role="all", category="champion", page=""):
        url = "https://www.mobafire.com/league-of-legends/browse"
        params = {
            "champion": champion,
            "role": role,
            "category": category,
            "sort": "top",
            "page": page,
        }

        return requests.get(url, params).content
