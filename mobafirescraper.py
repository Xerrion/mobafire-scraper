from bs4 import BeautifulSoup
from requester import Requester
import pendulum


class MobafireScraper(Requester):
    def __init__(self, champion="", role="", category="", page=1):
        self.request = Requester
        self.champion = champion
        self.role = role
        self.category = category
        self.page = page
        self.mobafire_url = "https://www.mobafire.com"

    def __champion_html(self):
        request = self.request.get_guides(
            champion=self.champion,
            role=self.role,
            category=self.category,
            page=self.page,
        )
        html = BeautifulSoup(request, "lxml")
        content = html.find(id="content")
        return content

    @staticmethod
    def __browse_list(html):
        return html.find("div", {"class": "browse-list"})

    def __list_items(self, html=""):
        return self.__browse_list(html).find_all("a", {"class": "browse-list__item"})

    def __get_pages(self):
        if self.__champion_html().find("div", {"class": "browse-pager__pages"}) is None:
            pages = 1
        else:
            pages = int(self.__champion_html().find("div", {"class": "browse-pager__pages"}).find_all("a")[-1].text)
        return pages

    def generate(self):
        guide_list = []
        pages = self.__get_pages() + 1

        try:
            for page in range(1, pages):
                print(f"Writing page {page}")

                request = self.request.get_guides(
                    champion=self.champion,
                    role=self.role,
                    category=self.category,
                    page=page,
                )
                html = BeautifulSoup(request, "lxml")
                content = html.find(id="content")

                for guide in self.__list_items(content):
                    timestamp = int(
                        guide.find("span", class_="localized-datetime")["data-timestamp"]
                    )
                    title = guide.find("h3", class_="browse-list__item__desc__title").text
                    description = guide.find(
                        "span", class_="browse-list__item__desc__meta"
                    ).text.replace("\n", " ")
                    url = self.mobafire_url + guide["href"]
                    votes = guide.find(
                        "div", class_="browse-list__item__rating__total"
                    ).text.replace("Votes: ", "")
                    views = guide.find(
                        "div", class_="browse-list__item__rating__views"
                    ).text.replace("Views: ", "")
                    updated_at = pendulum.from_timestamp(timestamp).format("DD-MM-YYYY")

                    guides_dict = {
                        "title": title,
                        "description": description,
                        "url": url,
                        "votes": votes,
                        "views": views,
                        "updated_at": updated_at,
                    }

                    guide_list.append(guides_dict)
            return guide_list
        except Exception as e:
            return e
