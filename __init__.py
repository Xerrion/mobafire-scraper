from mobafirescraper import MobafireScraper


def main():
    scrape = MobafireScraper(champion="udyr", role="jungle")
    with open("test.json", "w", encoding="utf-8") as json_file:
        import json

        result = json.dumps(scrape.generate())
        json_file.write(result)


if __name__ == "__main__":
    main()
